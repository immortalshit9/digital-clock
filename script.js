setInterval(() => {
    let hours = document.getElementById("hours");
    let minutes = document.getElementById("minutes");
    let seconds = document.getElementById("seconds");
    let ampm = document.getElementById("ampm");

    let hh = document.getElementById("hh");
    let mm = document.getElementById("mm");
    let ss = document.getElementById("ss");

    let hour_dot = document.querySelector(".hour_dot");
    let min_dot = document.querySelector(".min_dot");
    let sec_dot = document.querySelector(".sec_dot");

    let h = new Date().getHours();
    let m = new Date().getMinutes();
    let s = new Date().getSeconds();
    let am = h > 12 ? "PM" : "AM";

    if (h > 12) {
        h = h - 12;
    }

    h = h < 10 ? "0" + h : h;
    m = m < 10 ? "0" + m : m;
    s = s < 10 ? "0" + s : s;

    hours.innerHTML = h + "<br><span>Hours</span></br>";
    minutes.innerHTML = m + "<br><span>Minutes</span></br>";
    seconds.innerHTML = s + "<br><span>Seconds</span></br>";

    hh.style.strokeDashoffset = 600 - (600 * h) / 12;
    mm.style.strokeDashoffset = 600 - (600 * m) / 60;
    ss.style.strokeDashoffset = 600 - (600 * s) / 60;

    hour_dot.style.transform = `rotate(${h * 30}deg)`;
    min_dot.style.transform = `rotate(${m * 6}deg)`;
    sec_dot.style.transform = `rotate(${s * 6}deg)`;
});

document
    .getElementById("change-theme-wrapper")
    .addEventListener("click", () => {
        event.preventDefault();
        if (localStorage.getItem("theme") === "dark") {
            localStorage.removeItem("theme");
        } else {
            localStorage.setItem("theme", "dark");
        }
        addDarkClassToHTML();
    });

function addDarkClassToHTML() {
    if (localStorage.getItem("theme") === "dark") {
        document.querySelector("html").classList.add("dark");
        document.getElementById("theme").textContent = "clear_night";
        document.getElementById("theme-button").textContent="Night theme";
        document.getElementById("logo").classList.add("dark");

    } else {
        document.querySelector("html").classList.remove("dark");
        document.getElementById("theme").textContent = "sunny";
        document.getElementById("theme-button").textContent="Light theme";
        document.getElementById("logo").classList.remove("dark");
    }
}


addDarkClassToHTML();
